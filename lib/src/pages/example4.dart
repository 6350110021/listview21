import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Example4 extends StatelessWidget {
  Example4({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];

  final images = [
    AssetImage('images/cat1.jpg'),
    AssetImage('images/cat2.jpg'),
    AssetImage('images/cat3.jpg'),
    AssetImage('images/cat4.jpg'),
    AssetImage('images/cat5.jpg'),
    AssetImage('images/cat6.jpg'),
    AssetImage('images/cat7.jpg'),
    AssetImage('images/cat8.jpg'),
    AssetImage('images/cat9.jpg')
  ];

  final subtitle = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('listView4'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: images[index],
                  radius: 30,
                ),
                title: Text(
                  '${titles[index]}',
                  style: TextStyle(fontSize: 18),
                ),
                subtitle: Text(
                  subtitle[index],
                  style: TextStyle(fontSize: 15),
                ),
                trailing: Icon(
                  Icons.notifications_none,
                  size: 25,
                ),
                onTap: (){
                  Fluttertoast.showToast(msg:'${titles[index]}',
                    toastLength: Toast.LENGTH_SHORT,
                  );
                },
              ),
              Divider(
                thickness: 1,
              ),
            ],
          );
        },
      ),
    );
  }
}
